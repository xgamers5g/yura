<?php 
namespace Controller;

use Model\User;

class UserController
{
    private $dbm = null;
    
    public function __construct()
    {
        $this->dbm = new User();
    }

    public function receiveMessage()
    {
        if (isset($_POST['title']) && isset($_POST['message'])) {
            $this->dbm->saveData($_POST['title'], $_POST['message']);
            $this->showAllMessages();
        }
    }

    public function showAllMessages()
    {
        $loader = new \Twig_Loader_Filesystem('template');
        $twig = new \Twig_Environment($loader);
        $msg = $this->dbm->loadData();
        $var = array ();

        while ($row = $msg->fetch()) {
            $var[]=$row;
        }

        $show = array( 'msg' => $var);
        echo $twig->render('view.html', $show);
    }
}
