<?php 
namespace Controller;

use Model\Manager;
use Model\User;

class ManagerController
{
    private $dbm = null;
    
    public function __construct()
    {
        $this->dbm = new User();
        $this->dbmm = new Manager();
    }

    public function delMessage()
    {
        if (isset($_POST['delete']) && isset($_POST['id'])) {
            $this->dbmm->delData($_POST['id']);
            $this->manager();
        }
    }

    public function replyMessage()
    {
        if (isset($_POST['reply']) && isset($_POST['id'])) {
            $this->dbmm->replyData($_POST['id']);
            $this->manager();
        }
    }

    public function manager()
    {
        $loader = new \Twig_Loader_Filesystem('template');
        $twig = new \Twig_Environment($loader);
        $msg = $this->dbm->loadData();
        $var = array ();

        while ($row = $msg->fetch()) {
            $var[]=$row;
        }

        $show = array( 'msg' => $var);
        echo $twig->render('manager.html', $show);
    }

    public function login()
    {
        $loader = new \Twig_Loader_Filesystem('template');
        $twig = new \Twig_Environment($loader);
        echo $twig->render('login.html');
    }

    public function send()
    {
        if (isset($_POST['acc']) && isset($_POST['pwd'])) {
            $msg = $this->dbmm->loginData($_POST['acc'], $_POST['pwd']);
            if ($msg == 1) {
                $this->manager();
            } else {
                echo '帳號或密碼錯誤，請重新登入';
                $this->login();
            }
        }
    }
}
