<?php
namespace Model;

use Model\Db;
use PDO;

class User extends Db
{
    public function __construct()
    {
            parent::__construct();
    }

    public function saveData($t, $m)
    {
        $add = $this->db->prepare(
            "INSERT INTO message (mdate, title, message) VALUE (now(), '$t', '$m');"
        );
        $msg = $add->execute();
    }

    public function loadData()
    {
        $msg = $this->db->query("SELECT * FROM message order by mdate desc");
        $msg -> setFetchMode(PDO::FETCH_ASSOC);
        return $msg;
    }
}
