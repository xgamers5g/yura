<?php
namespace Model;

use Model\Db;

class Manager extends Db
{
    public function __construct()
    {
            parent::__construct();
    }

    public function delData($id)
    {
        $del = $this->db->query("DELETE FROM message WHERE id = '$id'");
        return $del;
    }

    public function replyData($id)
    {
        $rep = $this->db->query("UPDATE message SET reply='已讀' WHERE id = '$id'");
        return $rep;
    }

    public function loginData($acc, $pwd)
    {
        $msg = $this->db->query("SELECT mstate FROM manager WHERE maccount = '$acc' and mpassword = '$pwd'");
        $row = $msg->fetchColumn();
        return $row;
    }
}
