<?php
namespace Model;

use PDO;

class Db
{
    public $db;
    public function __construct()
    {
        $this->db = $this->getConn('test', 'root', '1234');
    }
    
    public function getConn($dbname, $dbuser, $dbpwd)
    {
        return new PDO(
            "mysql:host=localhost;dbname=".$dbname,
            $dbuser,
            $dbpwd,
            array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8")
        );
    }
}
