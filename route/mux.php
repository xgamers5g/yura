<?php
use Pux\Mux;

$mux = new Pux\Mux;
$mux->get('/', ['Controller\UserController', 'showAllMessages']);
$mux->post('/', ['Controller\UserController', 'receiveMessage']);
$mux->post('/login/ymg/del', ['Controller\ManagerController', 'delMessage']);
$mux->post('/login/ymg/rep', ['Controller\ManagerController', 'replyMessage']);
$mux->post('/login/ymg', ['Controller\ManagerController', 'manager']);
$mux->get('/login', ['Controller\ManagerController', 'login']);
$mux->post('/login', ['Controller\ManagerController', 'send']);
$mux->sort();
return $mux;
